
## Implementation of a demo user list app

### Project description

The purpose of this app is to understand how Ionic and Angular work by taking data

from a sample API (http://jsonplaceholder.typicode.com/users) and displaying a page with a list of items

and another page with the details. Of course clicking on an item on the list will take you to the details page.

