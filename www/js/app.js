// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var userApp = angular.module('starter', ['ionic']);


userApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('list', {
        url: '/',
        templateUrl: 'list.html',
        controller: 'usersCtrl'
      })
      .state('view', {
        url: '/userList/:id',
        templateUrl: 'view.html',
        controller: function($stateParams, $scope) {
          //$scope.usersItems = $scope.usersItems;
          $scope.params = $stateParams;
        }
      });

    $urlRouterProvider.otherwise("/");

  })

userApp.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });

})


userApp.controller('usersCtrl', function ($rootScope, $timeout, $http) {
/*
  var xmlhttp = new XMLHttpRequest();
  var url = "https://dl.dropboxusercontent.com/s/eio0wkkkrkvc4il/json.txt";

  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      //myFunction(xmlhttp.responseText);

      $timeout(function(){
        $rootScope.usersItems = JSON.parse(xmlhttp.responseText);
      },2000)
    }
  }
  if($rootScope.noReceived == undefined) {
    $rootScope.usersItems = [{name:"Loading Database..."}];
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    $rootScope.noReceived = true;
    //console.log($scope.usersItems);
  }*/
  if($rootScope.noReceived == undefined) {
    $rootScope.usersItems = [{name:"Loading Database..."}];
    $http({
      method: 'GET',
      url: "https://dl.dropboxusercontent.com/s/eio0wkkkrkvc4il/json.txt"
    }).then(function successCallback(response) {
      $timeout(function () {
        $rootScope.noReceived = true;
        $rootScope.usersItems = angular.fromJson(response.data);
      }, 2000)
    }, function errorCallback(response) {
      console.log("Error");
    });
  }
});



